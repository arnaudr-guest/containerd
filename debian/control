Source: containerd
Section: admin
Priority: optional
Maintainer: Debian Go Packaging Team <pkg-go-maintainers@lists.alioth.debian.org>
Uploaders: Arnaud Rebillout <arnaud.rebillout@collabora.com>,
           Tianon Gravi <tianon@debian.org>,
           Tim Potter <tpot@hpe.com>
Build-Depends: debhelper (>= 11),
               dh-golang,
               golang-any (>= 2:1.10~),
               golang-github-boltdb-bolt-dev,
               golang-github-burntsushi-toml-dev,
               golang-github-containerd-btrfs-dev,
               golang-github-containerd-cgroups-dev,
               golang-github-containerd-console-dev,
               golang-github-containerd-continuity-dev,
               golang-github-containerd-fifo-dev,
               golang-github-containerd-go-runc-dev,
               golang-github-containerd-typeurl-dev,
               golang-github-docker-go-events-dev (>= 0.0~git20170721~),
               golang-github-docker-go-metrics-dev,
               golang-github-docker-go-units-dev,
               golang-github-gogo-protobuf-dev,
               golang-github-grpc-ecosystem-go-grpc-prometheus-dev,
               golang-github-opencontainers-go-digest-dev,
               golang-github-opencontainers-image-spec-dev (>= 1.0.0-rc4),
               golang-github-opencontainers-runc-dev,
               golang-github-opencontainers-specs-dev,
               golang-github-pkg-errors-dev,
               golang-github-prometheus-client-golang-dev,
               golang-github-seccomp-libseccomp-golang-dev,
               golang-github-sirupsen-logrus-dev (>= 1.0.2~)
               golang-github-stevvooe-ttrpc-dev,
               golang-github-stretchr-testify-dev,
               golang-github-urfave-cli-dev (>= 1.20~),
               golang-gocapability-dev,
               golang-golang-x-net-dev,
               golang-golang-x-sync-dev,
               golang-golang-x-sys-dev,
               golang-google-genproto-dev,
               golang-google-grpc-dev,
               golang-goprotobuf-dev,
               libapparmor-dev
Standards-Version: 4.1.3
Homepage: https://github.com/containerd/containerd
Vcs-Git: https://anonscm.debian.org/git/pkg-go/packages/containerd.git
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-go/packages/containerd.git
XS-Go-Import-Path: github.com/containerd/containerd
Testsuite: autopkgtest-pkg-go

Package: containerd
Architecture: any
Built-Using: ${misc:Built-Using}
Breaks: docker-containerd
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: Open and reliable container runtime
 containerd is an industry-standard container runtime with an emphasis
 on simplicity, robustness and portability. It is available as a daemon
 for Linux and Windows, which can manage the complete container lifecycle
 of its host system: image transfer and storage, container execution and
 supervision, low-level storage and network attachments, etc.
 .
 containerd is designed to be embedded into a larger system, rather than
 being used directly by developers or end-users.

Package: golang-github-containerd-containerd-dev
Architecture: all
Breaks: golang-github-docker-containerd-dev
Depends: ${misc:Depends},
         golang-github-boltdb-bolt-dev,
         golang-github-burntsushi-toml-dev,
         golang-github-containerd-btrfs-dev,
         golang-github-containerd-cgroups-dev,
         golang-github-containerd-console-dev,
         golang-github-containerd-continuity-dev,
         golang-github-containerd-fifo-dev,
         golang-github-containerd-go-runc-dev,
         golang-github-containerd-typeurl-dev,
         golang-github-docker-go-events-dev (>= 0.0~git20170721~),
         golang-github-docker-go-metrics-dev,
         golang-github-docker-go-units-dev,
         golang-github-gogo-protobuf-dev,
         golang-github-grpc-ecosystem-go-grpc-prometheus-dev,
         golang-github-opencontainers-go-digest-dev,
         golang-github-opencontainers-image-spec-dev (>= 1.0.0-rc4),
         golang-github-opencontainers-runc-dev,
         golang-github-opencontainers-specs-dev,
         golang-github-pkg-errors-dev,
         golang-github-prometheus-client-golang-dev,
         golang-github-seccomp-libseccomp-golang-dev,
         golang-github-sirupsen-logrus-dev (>= 1.0.2~),
         golang-github-stevvooe-ttrpc-dev,
         golang-github-stretchr-testify-dev,
         golang-github-urfave-cli-dev (>= 1.20~),
         golang-gocapability-dev,
         golang-golang-x-net-dev,
         golang-golang-x-sync-dev,
         golang-golang-x-sys-dev,
         golang-google-genproto-dev,
         golang-google-grpc-dev,
         golang-goprotobuf-dev,
         libapparmor-dev
Description: Externally reusable Go packages included with Containerd
 These packages are intentionally developed by upstream in such a way that they
 are reusable to projects outside Continuity and only rely on each other or
 other external dependencies to be built.
